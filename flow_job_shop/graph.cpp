﻿#include "graph.h"

Graph::Graph()
{
	adjacentyMatrix = nullptr;
	verticeCount = 0;
}

Graph::Graph(int jobs, int machines)
{
	jobs_ = jobs;
	machines_ = machines;
	verticeCount = machines_ * jobs_ ;
	adjacentyMatrix = make_unique<int[]>(verticeCount*verticeCount);
}

Graph::Graph(int jobs, int machines, bool x )
{
	jobs_ = jobs;
	machines_ = machines;
	verticeCount = machines_ * jobs_ + 1;
	adjacentyMatrix = make_unique<int[]>(verticeCount*verticeCount);
}


Graph::~Graph()
{
}

void Graph::oneWayTicket()
{
	int line, column;
	for (line = 0; line < verticeCount; ++line)
	{
		for (column = 0; column < verticeCount; ++column)
		{
			if (line > column)
			{
				adjacentyMatrix[line*verticeCount + column] = 0;
			}
		}
	}
}

void Graph::createVerticeFlowShopBelman(int i, int edgeValue)
{
	int size = jobs_;//straznik uniemozliwajacy wyjscie pozazakres macierzy sasiedztwa

	if (i + 1 < verticeCount && (i + 1) % size != 0)
		setEdgeValue(i, i + 1, edgeValue);

	if (i == verticeCount - 1 - size) //blokada laczenia
	{
		//chcemy polaczyc nasz dodatkowy wierzcholek wirtualny z wierzcholkiem przedostatnim
		//a z wierzcholkiem numer verticeCount - 1 - size
		return;
	}

	if (i == verticeCount - 1) //laczenie ostaniego wierzcholka z wirtualnym
		setEdgeValue(i-1, i , edgeValue);

	if (i + size < verticeCount) 
		setEdgeValue(i, i + size, edgeValue);
}

void Graph::createVerticeFlowShop(int i, int edgeValue)
{
	int size = jobs_;

	if (i + 1 < verticeCount && (i + 1) % size != 0)
		setEdgeValue(i, i + 1, edgeValue);

	if (i + size < verticeCount)
		setEdgeValue(i, i + size, edgeValue);
}

void Graph::createVerticeJobShop(int i, int delta, int edgeValue)
{
	int size = jobs_;
	if (delta == 0 && i + 1 < verticeCount && (((i + 1) % size) != 0))
	{
		setEdgeValue(i, i + 1, edgeValue);
		//cout << i << " z " << i + 1 << endl;
	}

	if (i + size < verticeCount && delta == -1)
	{
		setEdgeValue(i, i + (-1)*delta*size, edgeValue);
		//cout << i << " z " << i + size << endl;
	}

	if (i + (-1)*delta*size < verticeCount && delta < -1)
	{
		setEdgeValue(i, i + (-1)*delta*size, edgeValue);
		//cout << i << " z " << i + (-1)*delta*size << endl;
	}

	if (i - size > 0 && delta == 1)
	{
		setEdgeValue(i + size, i + size - size, edgeValue);
		//cout << i + size << " z " << i + size - size << endl;
	}

	if (i + delta*size < verticeCount && delta > 1)
	{
		setEdgeValue(i + size*delta, i + size - size, edgeValue);
		//cout << i + size*delta << " z " << i + size - size*delta << endl;
	}
}

void Graph::addEdge(int vertice, int edgeValue)
{
	adjacentyMatrix[verticeCount*(verticeCount - 1) + vertice] = edgeValue;
	adjacentyMatrix[verticeCount*vertice + verticeCount - 1] = edgeValue;
	oneWayTicket();
}

void Graph::addEdges()
{
    int vertice=0;
    for(vertice = 0; vertice < (verticeCount-1); ++vertice)
    {
        addEdge(vertice, (1+vertice)*10);           // zamiast (1+vertice)*10 powinna byc wartosc odpowiedniego czasu dla krawedzi
    }
}

void Graph::pushVertice(string nameOfVertex)
{
    ++verticeCount;
    vertices.push_back(nameOfVertex);
    auto newMatrix = make_unique<int[]>(verticeCount*verticeCount);
    rewritingMatrix(move(newMatrix));
    addEdges();
}

void Graph::rewritingMatrix(unique_ptr<int[]> newMatrix)
{
    int oldVerticeCount = verticeCount - 1;

    int line, column;
    for(line=0; line<oldVerticeCount; ++line)
    {
        for(column=0; column<oldVerticeCount; ++column)
        {
            newMatrix[line*verticeCount + column] = adjacentyMatrix[line*oldVerticeCount + column];
        }
    }
    for(column=0; column<verticeCount; ++column)
    {
        newMatrix[verticeCount*verticeCount - column - 1] = 0;
        newMatrix[verticeCount*column + verticeCount - 1] = 0;
    }
    adjacentyMatrix = move(newMatrix);
}

void Graph::showAdjacentyMatrix()
{
    int line, column;
    for(line=0; line<verticeCount; ++line)
    {
        for(column=0; column<verticeCount; ++column)
        {
            cout << adjacentyMatrix[line*verticeCount + column] << "\t";
        }
        cout << endl;
    }
}

void Graph::showEdge(string firstVertice, string secondVertice)
{
    int oneVertice = getVerticeNumber(firstVertice);
    int twoVertice = getVerticeNumber(secondVertice);
    int edgeValue = getEdgeValue(oneVertice, twoVertice);
    //cout << edgeValue << ":   " <<  vertices.at(oneVertice) << "===" << vertices.at(twoVertice) << endl;
}

int Graph::getEdgeValue(int firstVertice, int secondVertice)
{
    return adjacentyMatrix[firstVertice*verticeCount + secondVertice];
}

void Graph::setEdgeValue(int firstVertice, int secondVertice, int size)
{
	adjacentyMatrix[firstVertice*verticeCount + secondVertice] = size;
}

int Graph::getVerticeNumber(string name)
{
    int vertiveNumber = 0;
    for(auto i : vertices)
    {
        if(name == i)
        {
            return vertiveNumber;
        }
        ++vertiveNumber;
    }
    return -999;
}

void Graph::showVerticle(int vertice)
{
    int edgeVertice;
    for(edgeVertice=0; edgeVertice<verticeCount; ++edgeVertice)
    {
        if(edgeVertice != vertice)
        {
            int edgeValue = adjacentyMatrix[vertice*verticeCount + edgeVertice];
            if(0 != edgeValue)
            {
//                cout << vertices.at(vertice) << "===" << vertices.at(edgeVertice) << ":\t" << edgeValue << endl;
            }
        }
    }
}

int Graph::totalTime()
{
	int size = jobs_;
	int *d = new int[verticeCount];
	int *p = new int[verticeCount];
	for (int i = 0; i < verticeCount; i++) // ustawiamy wartosci poczatkowe w tablicach pomocniczych
	{
		d[i] = -1;
		p[i] = -1;
	}

	d[0] = 0; //zerujemy pierwsze pole

	for (int i = 0; i < verticeCount - 1; i++)
	{
		int tmp = d[i];
		if ((i + 1) < verticeCount  && adjacentyMatrix[i*verticeCount + (i + 1)] != 0) //sprawdzamy czy miescimy sie w zakresie macierzy
		{

			if (d[i + 1] < tmp + adjacentyMatrix[i*verticeCount + (i + 1)])
			{
				d[i + 1] = tmp + adjacentyMatrix[i*verticeCount + (i + 1)];
				p[i + 1] = i + 1;	
			}
		}
		
		if ((i + size) < verticeCount  && adjacentyMatrix[i*verticeCount + (i + size)] != 0) //sprawdzamy czy miescimy sie w zakresie macierzy
		{
			if (d[i + size] < tmp + adjacentyMatrix[i*verticeCount + (i + size)])
			{
				d[i + size] = tmp + adjacentyMatrix[i*verticeCount + (i + size)];
				p[i + size] =  i + size;
			}
		}
	}

	int totalTime = -1;
	for (int i = 0; i < verticeCount; i++)
	{
		//cout << d[i] << endl;
		if (totalTime < d[i])
		{
			totalTime = d[i];
		}
	}

	delete[] d;
	delete[] p;
	return totalTime;
}

int Graph::BelmanFord()
{
	int size = jobs_;
	int *d = new int[verticeCount];
	int *p = new int[verticeCount];
	for (int i = 0; i < verticeCount; i++) // ustawiamy wartosci poczatkowe w tablicach pomocniczych
	{
		d[i] = -1;
		p[i] = -1;
	}

	d[0] = 0; //zerujemy pierwsze pole

	for (int i = 0; i < verticeCount - 1; i++)
	{
		int tmp = d[i];
		if ((i + 1) < verticeCount  && adjacentyMatrix[i*verticeCount + (i + 1)] != 0) //sprawdzamy czy miescimy sie w zakresie macierzy
		{

			if (d[i + 1] < tmp + adjacentyMatrix[i*verticeCount + (i + 1)])
			{
				d[i + 1] = tmp + adjacentyMatrix[i*verticeCount + (i + 1)];
				p[i + 1] = i + 1;
			}
		}

		if ((i + size) < verticeCount  && adjacentyMatrix[i*verticeCount + (i + size)] != 0) //sprawdzamy czy miescimy sie w zakresie macierzy
		{
			if (d[i + size] < tmp + adjacentyMatrix[i*verticeCount + (i + size)])
			{
				d[i + size] = tmp + adjacentyMatrix[i*verticeCount + (i + size)];
				p[i + size] = i + size;
			}
		}
	}

	int totalTime = -1;
	for (int i = 0; i < verticeCount; i++)
	{
		//cout << d[i] << endl;
		if (totalTime < d[i])
		{
			totalTime = d[i];
		}
	}

	delete[] d;
	delete[] p;
	return totalTime;
}
