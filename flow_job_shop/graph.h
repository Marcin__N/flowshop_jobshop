﻿#pragma once
#include <iostream>
#include <vector>
#include <math.h>
#include <memory>

using std::cout;
using std::cin;
using std::endl;
using std::vector;
using std::unique_ptr;
using std::string;
using namespace std;

class Graph
{
    vector<string> vertices;                                    // oznaczenia poszczegolnych wierzcholkow
	unique_ptr<int[]> adjacentyMatrix;                          // jest to ilosc elementow w wierszu * ilosc wierszy, szybsze dzialanie niz na podwojnych wskaznikach
	int verticeCount;                                           // oznacza ilosc wierzchołków ( wierszy ),
	int jobs_;													//liczba zadan
	int machines_;												//liczba maszyn
    void addEdges();                                            // iteruje dodawanie krawedzi pomiedzy wierzcholkami
    void rewritingMatrix(unique_ptr<int[]> newMatrix);
    int getVerticeNumber(string name);
    int getEdgeValue(int firstVertice, int secondVertice);
	void setEdgeValue(int firstVertice, int secondVertice, int size);
    void oneWayTicket();

public:
	void addEdge(int vertice, int edgeValue);
    void pushVertice(string nameOfVertex);                      // dodaje pojedynczy wierzcholek oraz krawedzie
    void showAdjacentyMatrix();                                 // pokazuje macierz sasiedztwa
    void showEdge(string firstVertice, string secondVertice);   // pokazuje wartosc krawedzi i wierzcholki ktore laczy
    void showVerticle(int vertice);                             // pokazuje wszystkie polaczenia wierzcholka i krawedzie miedzy nimi
	void createVerticeFlowShop(int i, int edgeValue);
	void createVerticeJobShop(int i, int delta, int edgeValue);
	void createVerticeFlowShopBelman(int i, int edgeValue);
	int totalTime();											// oblicza i zwraca czas potrzebny do wykonania wszystkich zadan
	int BelmanFord();
	Graph();													//konstruktor domyslny grafu
	Graph(int jobs, int machines);								 //konstruktor na potrzeby zadania
	Graph::Graph(int jobs, int machines, bool x); //konstruktor na potrzeby zadania z Belmanem
	~Graph();

};
