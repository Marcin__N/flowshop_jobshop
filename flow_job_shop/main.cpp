﻿#include <iostream>
#include "graph.h"
#include <fstream>
#include <string>
#include <windows.h>  //do timera

using namespace std;
void readFlowShopData(); //wczytywanie danych dla flow shop	
void readJobShopData();	//wczytywanie danych dla job shop
void FlowShop(Graph &, int**, int, int); //tworzy graf i oblicza czas dla flow shop
void JobShop(Graph &, int**, int, int);//tworzy graf i oblicza czas dla job shop
void show(int**, int, int); // wysiwetlanie tablciy 2D ( do testow)
void readFlowShopBelmanData(); //wczytywanie danych dla flow shop	
void FlowShopBelman(Graph &, int**, int, int);


int main()
{
	//readJobShopData();
	readFlowShopData();
	readFlowShopBelmanData();
	cout << " KONIEC " << endl;
	cin.get();
	cin.get();
    return 0;
}


/*klasa definijaca timer, podaje czas w ms */
class timer
{
	LARGE_INTEGER frequency;        // ticks per second
	LARGE_INTEGER t1, t2;			// ticks
	double elapsedTime;

public:
	timer()
	{
		QueryPerformanceFrequency(&frequency);
		elapsedTime = 0;
	}
	void start()
	{
		QueryPerformanceCounter(&t1);
	}
	void stop()
	{
		QueryPerformanceCounter(&t2);
	}
	double getTime()
	{
		elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
		return elapsedTime;
	}
};

void FlowShop(Graph & obj, int **tab, int a, int b)
{
	int czas = 0;
	int tmpNumber = 0; // pomocnicza do numeru wierzcholka
	cout << endl;
	for (int j = 0; j < b; j++) //zamiana kolumn z wierszami, bo taka przyjelismy konwencje
	{ 
		for (int i = 0; i < a; i++) 
		{
			obj.createVerticeFlowShop(tmpNumber, tab[i][j * 2 + 1]);
			tmpNumber++;
		}

	}
	timer czasomierz;
	czasomierz.start();
	czas += obj.totalTime(); //obliczamy czas
	czas += tab[a - 1][b * 2 - 1]; //dodajemy czas ostniego elementu
	czasomierz.stop();
	cout << "Dlugosc drogi : " << czas << endl;
	cout << "Czas oblcizen : " << czasomierz.getTime() << endl;
	ofstream file;
	file.open("wynikiFlow.txt", ios::app | ios::out);
	file << czas; //dlugosc
	file << " ";
	file << czasomierz.getTime(); //czas oblcizen
	file << '\n';
}

void JobShop(Graph & obj, int **tab, int a, int b)
{
	int droga = 0;
	int tmpNumber = 0; // pomocnicza do numeru wierzcholka
	for (int j = 0; j < b; j++) //zamiana kolumn z wierszami, bo taka przyjelismy konwencje
	{
		for (int i = 0; i < a; i++)
		{
			//laczenie wierzcholkow w poziomie 
			obj.createVerticeJobShop(tmpNumber, 0, tab[i][j * 2 + 1]);

			//jeśli kolejność maszyn jest zachowana rosnąco
			if (j*2 + 2 != b*2 && tab[i][j * 2] - tab[i][j * 2 + 2] == -1)
			{
				int delta = tab[i][j * 2] - tab[i][j * 2 + 2];
				obj.createVerticeJobShop(tmpNumber, delta, tab[i][j * 2 + 1]);
			}

			//jeśli kolejność maszyn jest zachowana malejąco
			else if (j * 2 + 2 != b * 2 && tab[i][j * 2] - tab[i][j * 2 + 2] == 1)
			{
				int delta = tab[i][j * 2] - tab[i][j * 2 + 2];
				obj.createVerticeJobShop(tmpNumber, delta, tab[i][j * 2 + 1]);
			}
			
			//jeśli inna kolejnosc, ale rosnaca
			 else if (j * 2 + 2 != b * 2 && tab[i][j * 2] - tab[i][j * 2 + 2] < -1)
			{
				int delta = tab[i][j * 2] - tab[i][j * 2 + 2];
				obj.createVerticeJobShop(tmpNumber, delta, tab[i][j * 2 + 1]);
			}

			//jesli kolejnosc inna, ale malejaca
			 else if (j * 2 + 2 != b * 2 && tab[i][j * 2] - tab[i][j * 2 + 2] > 1)
			 {
				 int delta = tab[i][j * 2] - tab[i][j * 2 + 2];
				 obj.createVerticeJobShop(tmpNumber, delta, tab[i][j * 2 + 1]);
			 }
				tmpNumber++;
		}
	}
	timer czasomierz; 
	czasomierz.start(); //wlaczamy timer
	droga += obj.totalTime(); //obliczamy droge
	droga += tab[a - 1][b * 2 - 1]; //dodajemy czas ostniego elementu
	czasomierz.stop(); // zatrzymujemy timer
	cout << "Dlugosc drogi : " << droga << endl;
	cout << "Czas oblcizen : " << czasomierz.getTime() << endl;

	ofstream file;
	file.open("wynikiJob.txt", ios::app|ios::out); //zapisywanie na koniec pliku
	file << droga; // dlugosc
	file << " ";
	file << czasomierz.getTime(); //czas oblcizen
	file << '\n';

}

void readFlowShopData()
{
	ifstream file;
	file.open("flow.txt", ios::in);
	if (!file.is_open())
	{
		cout << "Otwarcie pliku nie powiodlo sie." << endl;
		return ;
	}
	int size;
	file >> size; //pobieranie rozmiaru z naglowka pliku
	for (int i = 0; i < size; i++)
	{
		/*Wczytywanie z pliku do tablicy*/
		int a, b;
		file >> a; // pobieramy ilosc zadan
		file >> b; // pobieramy ilosc maszyn
		int **tab = new int*[a];
		for (int j = 0; j < a; j++)
		{
			tab[j] = new int[b * 2];
			for (int k = 0; k < b * 2; k++)
				file >> tab[j][k];
		}
		Graph g1(a,b); //tworze pusty graf a*b wierzcholkach
		FlowShop(g1, tab, a, b); //wypelnianie grafu i obliczanie czasu
		 /*Zwalnianie pamiecy po tablicy z danymi*/
		for (int i = 0; i < a; i++)
			delete[] tab[i];
		delete[] tab;
	}
}

void readJobShopData()
{
	ifstream file;
	file.open("job.txt", ios::in);
	if (!file.is_open())
	{
		cout << "Otwarcie pliku nie powiodlo sie." << endl;
		return;
	}

	for (int i = 0; i < 80; i++) // bo 80 instancji w pliku
	{
		/*Wczytywanie z pliku do tablicy*/
		string tmp; //przy flowshop nie pobieramy, bo tak w pliku tail.txt nie ma nazw
		getline(file, tmp); //pobieramy nazwe instancji
		int a, b;
		file >> a; // pobieramy ilosc zadan
		file >> b; // pobieramy ilosc maszyn
		int **tab = new int*[a];
		for (int j = 0; j < a; j++)
		{
			tab[j] = new int[b * 2];
			for (int k = 0; k < b * 2; k++)
				file >> tab[j][k];
		}
		Graph g1(a,b); //tworze pusty graf a*b wierzcholkach
		JobShop(g1, tab, a, b); //wypelnianie grafu i oblcizenia
		file >> tmp; //pobieranie pustego wiercza pod instancja
		/*Zwalnianie pamiecy po tablicy z danymi*/
		for (int i = 0; i < a; i++)
			delete[] tab[i];
		delete[] tab;
	}
}

void readFlowShopBelmanData()
{
	ifstream file;
	file.open("flow.txt", ios::in);
	if (!file.is_open())
	{
		cout << "Otwarcie pliku nie powiodlo sie." << endl;
		return;
	}
	int size;
	file >> size; //pobieranie rozmiaru z naglowka pliku
	for (int i = 0; i < size; i++)
	{
		/*Wczytywanie z pliku do tablicy*/
		int a, b;
		file >> a; // pobieramy ilosc zadan
		file >> b; // pobieramy ilosc maszyn


		int **tab = new int*[a];
		for (int j = 0; j < a; j++)
		{
			tab[j] = new int[b * 2];
			for (int k = 0; k < b * 2; k++)
				file >> tab[j][k];
		}

		Graph g1(a, b, 1); //tworze pusty graf a*b wierzcholkach
		FlowShopBelman(g1, tab, a, b); //wypelnianie grafu i obliczanie czasu
								 /*Zwalnianie pamiecy po tablicy z danymi*/
		for (int i = 0; i < a; i++)
			delete[] tab[i];
		delete[] tab;
	}
}

void FlowShopBelman(Graph & obj, int **tab, int a, int b)
{
	int czas = 0;
	int tmpNumber = 0; // pomocnicza do numeru wierzcholka
	cout << endl;
	for (int j = 0; j < b; j++) //zamiana kolumn z wierszami, bo taka przyjelismy konwencje
	{
		for (int i = 0; i < a; i++)
		{
			obj.createVerticeFlowShopBelman(tmpNumber, tab[i][j * 2 + 1]);
			tmpNumber++;
		}

	}
	obj.createVerticeFlowShopBelman(tmpNumber, tab[a - 1][b * 2 - 1]);//dodawanie wirtualnego wierzchołka
	
	timer czasomierz;
	czasomierz.start();
	czas += obj.BelmanFord(); //obliczamy czas
	czasomierz.stop();
	cout << "Dlugosc drogi : " << czas << endl;
	cout << "Czas oblcizen : " << czasomierz.getTime() << endl;
	ofstream file;
	file.open("wynikiFlowBelman.txt", ios::app | ios::out);
	file << czas; //dlugosc
	file << " ";
	file << czasomierz.getTime(); //czas oblcizen
	file << '\n';
}

void show(int ** tab, int a, int b) 
{
	for (int i = 0; i < a; i++)
	{
		for (int j = 0; j < b * 2; j++)
			cout << tab[i][j] << " ";
		cout << endl;
	}
}